import { SignUp } from '@clerk/nextjs'
import AuthLayout from '../../AuthLayout'

export default function Page() {
  return (
    <AuthLayout>
      <SignUp />
    </AuthLayout>
  )
}
