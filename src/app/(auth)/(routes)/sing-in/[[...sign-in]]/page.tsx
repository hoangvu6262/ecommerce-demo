import { SignIn } from '@clerk/nextjs'
import AuthLayout from '../../AuthLayout'

export default function Page() {
  return (
    <AuthLayout>
      <SignIn />
    </AuthLayout>
  )
}
